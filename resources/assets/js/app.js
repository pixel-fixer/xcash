require('./bootstrap');
import Vue from 'vue';
import vuescroll from 'vuescroll'
import VueViewports from 'vue-viewports'
import IOdometer from 'vue-odometer';
import 'odometer/themes/odometer-theme-default.css';

Vue.use(vuescroll)

const options = [
    {
        rule: '320px',
        label: 'mobile'
    },
    {
        rule: '768px',
        label: 'tablet'
    },
    {
        rule: '1024px',
        label: 'laptop'
    },
    {
        rule: '1440px',
        label: 'desktop'
    }
]

Vue.use(VueViewports, options)

Vue.filter('numberformat', function(number, decimals, dec_point, thousands_sep) {
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function(n, prec) {
            var k = Math.pow(10, prec);
            return '' + (Math.round(n * k) / k)
                .toFixed(prec);
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
        .split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '')
            .length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1)
            .join('0');
    }
    return s.join(dec);
});

Vue.filter('formatPercent', function(number) {
    let parts = number.toFixed(1).toString().split('.');

    if(parts[1] == 0)
        return parts[0] + '<span>%</span>';
    else
        return parts[0] + '.<span>' + parts[1] +'%</span>';
});

Vue.filter('formatPercentChance', function(number) {
    let parts = number.toFixed(1).toString().split('.');
    return parts[0] + '.<span class="decimals">' + parts[1] +'%</span>';
});

// import './vue_filters'

window.Vue = Vue;

/* jquery scripts */

$(document).ready(function () {

    $('.lang-switch__button').click(function(){
        $(this).next('.lang-switch__list').toggle('fast');

    });

    $('body').click(function(e){
        e.stopPropagation();
        if ($(e.target).closest(".lang-switch__list").length === 0 &&
            $(e.target).closest(".lang-switch__button").length === 0) {
            $('.lang-switch__list').hide('fast');
        }
    });

    $(".lang-switch__list").mouseleave(function () {
        setTimeout(() => {
            $(this).hide('fast');
        }, 500);

    })
});

Vue.component('app', require('./components/app.vue'));
Vue.component('winners-list', require('./components/winners_list.vue'));
Vue.component('winner-one', require('./components/winner.vue'));
Vue.component('game', require('./components/game.vue'));
Vue.component('game-control', require('./components/game_control.vue'));
Vue.component('game-control2', require('./components/game_control2.vue'));
Vue.component('game-players', require('./components/game_players.vue'));
Vue.component('game-player', require('./components/game_player.vue'));
Vue.component('game-player-tickets', require('./components/game_player_tickets.vue'));
Vue.component('game-countdown', require('./components/game_countdown.vue'));
Vue.component('game-raffle', require('./components/game_raffle.vue'));
Vue.component('user-profile', require('./components/user_profile.vue'));
Vue.component('winner-popup', require('./components/winner_popup.vue'));
Vue.component('vue-odometer', IOdometer);
Vue.component('animated-number', require('./components/animated_number.vue'));
Vue.component('animated-chance', require('./components/animated_chance.vue'));
Vue.component('modal-sidebar', require('./components/modal_sidebar.vue'));
Vue.component('select-phone-code', require('./components/forms/select_phone_code.vue'));
Vue.component('form-phone', require('./components/forms/form_phone.vue'));
Vue.component('form-email', require('./components/forms/form_email.vue'));
Vue.component('form-confirm-sms', require('./components/forms/form_confirm_sms.vue'));
Vue.component('form-confirm-email', require('./components/forms/form_confirm_email.vue'));
Vue.component('notification', require('./components/notification.vue'));
Vue.component('form-avatar', require('./components/forms/form_avatar.vue'));
Vue.component('user-auth', require('./components/user_auth.vue'));
Vue.component('modal', require('./components/modal.vue'));
Vue.component('modal-nav', require('./components/modal_nav.vue'));
Vue.component('form-withdraw', require('./components/forms/withdraw.vue'));

const EventBus = new Vue();

Object.defineProperties(Vue.prototype, {
    $bus: {
        get: function () {
            return EventBus
        }
    }
});

const app = new Vue({
    el: '#app',
    data: {
        user:{},
        online: 678,
        showSideBar: false,
        sideBarType: null,
        mobileLinks: [
            {link: '/', name: 'Игра'},
            {link: '/bonus', name: 'Бонусы'},
            {link: '/rating', name: 'Рейтинг'},
            {link: '/payments', name: 'Выплаты'},
            {link: '/help', name: 'Помощь'},
        ],
        currentLink: window.location.pathname
    },
    created: function () {
        this.$bus.$on('open-sidebar', type => {
            this.sideBarType = type
            this.showSideBar = true
        });
        this.$bus.$on('close-sidebar', () => {
            this.showSideBar = false
        });
        this.getUser();
        //this.authUser();
    },
    methods: {
        getUser: function () {
            //axios.get(...)
            this.user = {}
        },
        authUser(){
            this.user = {
                id: 1,
                balance: 283,
                avatar: '/images/ava.jpg',
                name: 'Константин В.',
                messages: 5,
                tickets: []
            }
        }
    }
});
