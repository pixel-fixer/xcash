import Vue from 'vue'

let maxchars = Vue.directive("maxchars", {
    bind: function(el, binding, vnode) {
        var max_chars = binding.expression;
        var handler = function(e) {
            console.log(max_chars)
            if (e.target.value.length > max_chars) {
                // console.log(e);
                // e.preventDefault();
                //  e.target.value = e.target.value.substr(0, max_chars);
                // e.target.value ='';
            }
        };
        el.addEventListener("input", handler);
    }
});

export {maxchars};