<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>XCASH</title>
    <meta name="csrf-token" content="{{csrf_token()}}">
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
</head>
<body>
    <div class="overlay">
         <div id="app">
            <div class="page-wrap">
                @include('partials.header')
                @include('partials.edges')
                @yield('content')
                <div class="container">
                    <div class="seoText">
                        <div class="headline">Для роботов</div>
                        <div class="row">
                            <div class="col-xs-6 col-xs-12">
                                <p>Приложение для знакомств Tinder рассказало об огромном росте трафика в Олимпийской деревне во время соревнований. Например, с 8 февраля количество пользователей в Пхёнчхане выросло на 348 %, а мэтчей — на 644 %. В этом году спортсменам дали также бесплатные премиум-аккаунты. Резкий рост трафика зафиксировали и в Pornhub.</p>
                                <p>Британская компания Diageo представила Jane Walker — женскую версию скотча Johnnie Walker. Под брендом появится 250 тысяч бутылок скотча. Ограниченная серия виски поступит в продажу в марте. По одному доллару от каждой проданной бутылки пойдет в организации, помогающие женщинам.</p>
                            </div>
                            <div class="col-xs-6 col-xs-12">
                                <p>В основе сюжета — история одного из пожарных, сжигающих книги для того, чтобы переписать историю во времена мощного технологического прогресса. Главные роли в фильме исполнили Майкл Шэннон, Майкл Б. Джордан и София Бутелла. Премьера состоится уже в мае</p>
                                <p>Apple планирует выпустить в конце года три новых модели iPhone: обновленный iPhone X, бюджетную версию флагмана и самый большой смартфон из всех, что выпускала компания. Фаблет получит экран в 6,5 дюйма от края до края и, возможно, функцию разделения экрана и версию на две сим-карты. Вместе со смартфонами появятся и полноразмерные беспроводные наушники.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('partials.modals')
        </div>
    </div>
    <footer class="footer">
        <div class="container">
            <div class="row middle-xs footer__row">
                <div class="footer__copy col-xs">
                    © XCASH. Игра нового поколения.
                </div>
                <div class="footer__links col-xs">
                    <a href="" class="main-link">Подробнее</a>
                    <a href="" class="main-link">Конфиденциальность</a>
                </div>
                <div class="footer__socLinks col-xs">
                    <div class="footer__socLinks__outer">
                        <div>
                            <a href="">
                                <svg role="img" class="footer__socLinks__icon icon icon-vk">
                                    <use xlink:href="/img/icons.svg#icon-vk"></use>
                                </svg><span>Мы в Вконтакте</span>
                            </a>
                        </div>
                        <div>
                            <a href="">
                                <svg role="img" class="footer__socLinks__icon icon icon-telegram">
                                    <use xlink:href="/img/icons.svg#icon-telegram"></use>
                                </svg><span>Мы в Телеграм</span>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </footer>
</body>
<script src="{{mix('/js/app.js')}}"></script>
</html>
