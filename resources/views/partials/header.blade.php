<header class="header">
    <div class="container">
        <div class="header__content">
            <a href="/" class="header__logo-block">
                <img class="header__logo" src="/images/logo.svg" alt="">
            </a>
            <div class="header__field header__field--mobileTop">
                <div class="header__block header__block_left">
                    <div class="header__lang-block">
                        @include('partials.lang_swith')
                    </div>
                    <nav class="header__nav-block nav--full">
                        <span class="header__nav-link header__nav-link_active">
                            Игра
                        </span>
                        <a href="/" class="header__nav-link header__nav-link_with-notification">
                            Бонусы
                            <span class="header__badge">
                              <span class="header__badge-value">3</span>
                            </span>
                        </a>
                        <a href="/" class="header__nav-link">
                            Рейтинг
                        </a>
                        <a href="/" class="header__nav-link">
                            Выплаты
                        </a>
                        <a href="/" class="header__nav-link">
                            Помощь
                        </a>
                    </nav>
                    <nav class="header__nav-block nav--tablet">
                        <div>
                            <span class="header__nav-link header__nav-link_active">
                                Игра
                            </span>
                        </div>
                        <div>
                            <a href="/" class="header__nav-link header__nav-link_with-notification">
                                Бонусы
                                <span class="header__badge">
                                  <span class="header__badge-value">3</span>
                                </span>
                            </a>
                        </div>
                        <div class="nav__more">
                                <span>Еще</span>
                                <div class="nav__sub">
                                    <a href="/">
                                        Рейтинг
                                    </a>
                                    <a href="/">
                                        Выплаты
                                    </a>
                                    <a href="/">
                                        Помощь
                                    </a>
                                </div>
                        </div>
                    </nav>
                    <div class="nav-mobile">
                        <div class="nav-mobile__messages" v-if="typeof user.messages !== 'undefined'">@{{user.messages}}</div>
                        <div class="nav-mobile__open" @click="$bus.$emit('open-nav-mobile')"><span></span><span></span><span></span></div>
                    </div>
                </div>
            </div>
            <div class="header__field header__profile">
                <user-profile :user="user"></user-profile>
                <div style="position: absolute;z-index: 11;right:0;"><button @click="authUser">Авторизоваться</button></div>
            </div>
        </div>
    </div>
</header>