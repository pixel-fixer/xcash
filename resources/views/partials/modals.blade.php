<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="blur-svg">
    <defs>
        <filter id="blur-filter">
            <feGaussianBlur stdDeviation="3"></feGaussianBlur>
        </filter>
    </defs>
</svg>
<transition name="sidebar" :duration="500">
    <modal-sidebar v-if="showSideBar"  v-on:close="showSideBar=false" :type="sideBarType"></modal-sidebar>
</transition>

<modal id="edges" class-name="modal-edges" transition-name="edges" :transition-duration="700">
    <template slot="content">
        <section class="edges edges--popup">
            <div class="edges__wrapper">
                <div class="edges__item">
                    <img class="edges__item-img" src="/images/edge1.svg" alt="">
                    <span>XCASH проводит розыгрыши денег между игроками. Победитель забирает весь банк.</span>
                </div>
                <div class="edges__item">
                    <img class="edges__item-img" src="/images/edge2.svg" alt="">
                    <span>Люди делают ставки за 30 секунд. Больше ставка — выше шанс победы. Но, выиграть может и 1%.</span>
                </div>
                <div class="edges__item">
                    <img class="edges__item-img" src="/images/edge3.svg" alt="">
                    <span>Победителя выбирает Random.org. Честность любой игры <a href="#" class="main-link">можно проверить</a>.</span>
                </div>
            </div>
        </section>
        <div>
            <div class="btn btn--winner block" @click="$bus.$emit('close-edges')">ok, я понял</div>
        </div>
    </template>
</modal>

<modal-nav id="nav-mobile" transition-name="staggered-nav" class-name="modal-nav" :transition-duration="700">
    <template slot="content">
        <div class="modal-nav__menu">
            <div v-for="link in mobileLinks"
                 class="modal-nav__menu__item"
                 :class="{active: currentLink == link.link}">
                <span v-if="currentLink == link.link"></span>
                <a :href="link.link">@{{link.name}}</a>
            </div>
        </div>
    </template>
    <template slot="modal">
        <div class="modal-nav__profile" v-if="typeof user.avatar !== 'undefined'">
            <a href="/">
                <img :src="user.avatar" class="modal-nav__profile__ava">
                <span>Профиль</span>
            </a>
        </div>
    </template>
</modal-nav>

<notification></notification>

