<section class="edges edges--desktop">
    <div class="container container--pad">
        <div class="row">
            <div class="col-xs-12">
                <div class="edges__wrapper">
                    <div class="edges__modal-open" @click="$bus.$emit('open-edges')">Что такое xCash</div>
                    <div class="edges__item">
                        <img class="edges__item-img" src="/images/edge1.svg" alt="">
                        XCASH проводит розыгрыши денег между игроками. Победитель забирает весь банк.
                    </div>
                    <div class="edges__item">
                        <img class="edges__item-img" src="/images/edge2.svg" alt="">
                        Люди делают ставки за 30 секунд. Больше ставка — выше шанс победы. Но, выиграть может и 1%.
                    </div>
                    <div class="edges__item">
                        <img class="edges__item-img" src="/images/edge3.svg" alt="">
                        Победителя выбирает Random.org. Честность любой игры <a href="#" class="main-link">можно проверить</a>.
                    </div>
                    <div class="edges__online-counter">
                        <div class="edges__online-counter__outer">
                            <div class="edges__online-counter-value">
                                <img class="edges__online-counter-icon" src="/images/icon-online.svg" alt="">
                                <vue-odometer :value="online"></vue-odometer>
                            </div>
                            <div class="edges__online-counter-title">
                                <span v-if="$currentViewport.label === 'tablet'">сейчас онлайн</span>
                                <span v-else>Онлайн</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>