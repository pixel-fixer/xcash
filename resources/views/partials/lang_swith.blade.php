<div class="lang-switch">
    <button class="lang-switch__button">
        <img class="lang-switch__img" src="images/flag-ru.svg" alt="ru">
    </button>
    <ul class="lang-switch__list">
        <li class="lang-switch__list-item lang-switch__list-item_active">
            <img class="lang-switch__list-item-img" src="images/flag-ru.svg" alt="ru">
            <span class="lang-switch__list-item-text">
                                  Русский
                                </span>
            <img class="lang-switch__list-item-check" src="images/lang-check.svg" alt="">
        </li>
        <li class="lang-switch__list-item">
            <a href="/#us">
                <img class="lang-switch__list-item-img" src="images/flag-us.svg" alt="us">
                <span class="lang-switch__list-item-text">
                                  English
                                </span>
                <img class="lang-switch__list-item-check" src="images/lang-check.svg" alt="">
            </a>
        </li>
        <li class="lang-switch__list-item">
            <a href="/#ua">
                <img class="lang-switch__list-item-img" src="images/flag-ua.svg" alt="ua">
                <span class="lang-switch__list-item-text">
                                  Українська
                                </span>
                <img class="lang-switch__list-item-check" src="images/lang-check.svg" alt="">
            </a>
        </li>
    </ul>
</div>