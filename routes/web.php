<?php

Route::get('/', function () {
    return view('home');
});

Route::get('/api/phone_codes', function () {
    return [
        [
            'flag'=> 'images/flag-ru.svg',
            'code' => '7',
            'name' => 'Россия'
        ],
        [
            'flag'=> 'images/flag-us.svg',
            'code' => '1',
            'name' => 'США'
        ],
        [
            'flag'=> 'images/flag-ua.svg',
            'code' => '380',
            'name' => 'Украина'
        ],
        [
            'flag'=> 'images/flag-us.svg',
            'code' => '72',
            'name' => 'Казахстан'
        ],
        [
            'flag'=> 'images/flag-us.svg',
            'code' => '777',
            'name' => 'Казахстан'
        ],
        [
            'flag'=> 'images/flag-us.svg',
            'code' => '73',
            'name' => 'Казахстон'
        ]
    ];
});

Route::post('/api/auth/register/phone', function () {
    request()->validate([
        'phone' => 'required',
        'agree' => 'required|boolean'
    ]);

    return ['STATUS' => 'OK'];
});

Route::post('/api/auth/register/email', function () {
    request()->validate([
        'email' => 'required',
        'agree' => 'required|boolean'
    ]);

    return ['STATUS' => 'OK'];
});

Route::post('/api/auth/sms', function () {
    request()->validate([
        'code' => 'required'
    ]);

    return ['STATUS' => 'OK'];
});

Route::get('/api/auth/avatars', function () {
    return [
        [
            'id'=>1,
            'src'=>'/images/content/clown.png'
        ],
        [
            'id'=>2,
            'src'=>'/images/content/hmm.png'
        ],
        [
            'id'=>3,
            'src'=>'/images/content/money-eye.png'
        ],
        [
            'id'=>4,
            'src'=>'/images/content/nose.png'
        ],
        [
            'id'=>5,
            'src'=>'/images/content/pinokio.png'
        ],
        [
            'id'=>6,
            'src'=>'/images/content/skull.png'
        ],
        [
            'id'=>7,
            'src'=>'/images/content/pinokio.png'
        ],
        [
            'id'=>8,
            'src'=>'/images/content/hmm.png'
        ],
    ];
});

Route::post('/api/auth/register', function () {
    request()->validate([
        'login' => 'required'
    ]);

    return ['STATUS' => 'OK'];
});

Route::get('/api/withdraw/options', function () {
    return [
        [
            'id'=> 1,
            'icon'=>'/images/content/withdraw/bitcoin.png',
            'name' => 'Биткоин',
            'field_label' => 'кошелек',
            'penalty' => 1
        ],
        [
            'id'=> 2,
            'icon'=>'/images/content/withdraw/card.png',
            'name' => 'Visa, MasterCard',
            'field_label' => 'номер карты',
            'penalty' => 6
        ],
        [
            'id'=> 3,
            'icon'=>'/images/content/withdraw/qiwi.png',
            'name' => 'Qiwi',
            'field_label' => 'телефон',
            'penalty' => 0.3
        ],
        [
            'id'=> 4,
            'icon'=>'/images/content/withdraw/yandex-money.png',
            'name' => 'Bitcoin',
            'field_label' => 'номер кошелька',
            'penalty' => 3
        ],

    ];
});

Route::post('/api/withdraw/make', function () {
    request()->validate([
        'id' => 'required',
        'value' => 'required',
        'amount' => 'required'
    ]);

    return ['STATUS' => 'OK'];
});

