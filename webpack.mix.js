let mix = require('laravel-mix');
let SVGSpritemapPlugin = require('svg-spritemap-webpack-plugin');
let SpritesmithPlugin = require('webpack-spritesmith');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.webpackConfig({
    module: {
        loaders: [
            {test: /\.png$/, loaders: [
                'file-loader?name=i/[hash].[ext]'
            ]}
        ]
    },
    resolve: {
        modules: ["node_modules", "public/img", 'resources/assets/sass/sprite']
    },
    plugins: [
        new SVGSpritemapPlugin({
            src:'resources/assets/svg/*.svg',
            filename: '/img/icons.svg',
            prefix: 'icon-',
            deleteChunk: false,
            gutter: 10
        }),
        new SpritesmithPlugin({
            src: {
                cwd: path.resolve(__dirname, 'resources/assets/img/src'),
                glob: '*.png'
            },
            target: {
                image: path.resolve(__dirname, 'public/img/sprite.png'),
                css: path.resolve(__dirname,    'resources/assets/sass/_sprite.scss')
            },
            apiOptions: {
                cssImageRef: '~sprite.png'
            },
            spritesmithOptions: {
                padding: 20,
                algorithm: 'top-down'
            }
        }),
    ]
});


mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css')
    .version()
    .browserSync('xcash.loc')
    .disableNotifications();
